package ru.jokebot.jokes.exception;

public class EntityNotBeenCreatedException extends RuntimeException {

    public EntityNotBeenCreatedException(Object obj) {
        super("Category could not be created: " + obj.toString());
    }
}
