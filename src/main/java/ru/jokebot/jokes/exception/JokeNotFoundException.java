package ru.jokebot.jokes.exception;

public class JokeNotFoundException extends RuntimeException {

    public JokeNotFoundException() {
        super("Joke not found!");
    }

    public JokeNotFoundException(Long id) {
        super("Joke #%d not found: ".formatted(id));
    }

    public JokeNotFoundException(String text) {
        super("Joke \"%s\" not found!".formatted(text));
    }
}
