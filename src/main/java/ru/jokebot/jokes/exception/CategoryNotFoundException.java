package ru.jokebot.jokes.exception;

public class CategoryNotFoundException extends RuntimeException {

    public CategoryNotFoundException(Long id) {
        super("Category #%d not found: ".formatted(id));
    }

    public CategoryNotFoundException(String name) {
        super("Category #%s not found: ".formatted(name));
    }
}
