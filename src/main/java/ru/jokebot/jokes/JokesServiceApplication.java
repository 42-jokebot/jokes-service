package ru.jokebot.jokes;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@MapperScan(basePackages = "ru.jokebot.jokes.persistence.mapper")
@EnableFeignClients
public class JokesServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(JokesServiceApplication.class, args);
    }

}