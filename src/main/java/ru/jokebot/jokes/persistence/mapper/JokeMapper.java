package ru.jokebot.jokes.persistence.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import ru.jokebot.jokes.model.entity.Joke;

import java.util.Optional;

@Mapper
public interface JokeMapper {

    @Select("""
            SELECT id, text, category_id
            FROM jokes.joke
            WHERE text=#{text}
            """)
    Optional<Joke> getByText(String text);

    @Select("""
            SELECT id, text, category_id
            FROM jokes.joke
            WHERE id=#{id}
            """)
    Optional<Joke> getById(Long id);

    @Insert("""
            INSERT INTO jokes.joke (text, category_id)
            VALUES (#{text}, #{categoryId})
            """)
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void save(Joke joke);

    @Update("""
            UPDATE jokes.joke
            SET text = #{text}, category_id = #{categoryId}
            WHERE id=#{id}
            """)
    void update(Long id, Joke joke);

    @Delete("DELETE FROM jokes.joke WHERE id=#{id}")
    void deleteById(Long id);
}
