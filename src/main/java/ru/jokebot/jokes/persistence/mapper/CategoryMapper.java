package ru.jokebot.jokes.persistence.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import ru.jokebot.jokes.model.entity.Category;

import java.util.List;
import java.util.Optional;

@Mapper
public interface CategoryMapper {

    @Select("SELECT COUNT(id) FROM jokes.category")
    int getCategoriesCount();

    @Select("""
            SELECT id, name, locale
            FROM jokes.category
            WHERE locale=#{locale}
            """)
    List<Category> getAllByLocale(String locale);

    @Select("""
            SELECT id, name, locale
            FROM jokes.category
            WHERE name=#{name}
            """)
    Optional<Category> getByName(String name);

    @Select("""
            SELECT id, name, locale
            FROM jokes.category
            WHERE id=#{id}
            """)
    Optional<Category> getById(Long id);

    @Insert("""
            INSERT INTO jokes.category (name, locale)
            VALUES (#{name}, #{locale})
            """)
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void save(Category category);

    @Update("""
            UPDATE jokes.category
            SET name = #{name}, locale = #{locale}
            WHERE id=#{id}
            """)
    void update(Long id, Category category);

    @Delete("DELETE FROM jokes.category WHERE id=#{id}")
    void deleteById(Long id);
}
