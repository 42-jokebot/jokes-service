package ru.jokebot.jokes.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.jokebot.jokes.model.dto.JokeCreateUpdateDto;

@FeignClient(name = "integration-service")
public interface IntegrationServiceClient {

    @GetMapping("/api/v1/requestJoke")
    JokeCreateUpdateDto requestJoke(@RequestParam String category, @RequestParam String locale);
}
