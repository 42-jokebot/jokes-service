package ru.jokebot.jokes.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@FeignClient(name = "statistics-service")
public interface StatisticsServiceClient {

    @GetMapping("/api/v1/jokes/{categoryId:\\d+}/non-viewed")
    List<Long> getNotViewedJokesByCategory(@PathVariable Long categoryId, @RequestParam Long userId);

    @GetMapping("/api/v1/categories/rating")
    Map<Long, Integer> getReactedCategories();
}
