package ru.jokebot.jokes.model.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import ru.jokebot.jokes.model.dto.CategoryReadDto;
import ru.jokebot.jokes.model.entity.Category;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface CategoryReadEntityMapper {

    @Mapping(source = "name", target = "categoryName")
    CategoryReadDto map(Category category);
}