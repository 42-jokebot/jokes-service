package ru.jokebot.jokes.model.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import ru.jokebot.jokes.model.dto.JokeReadDto;
import ru.jokebot.jokes.model.entity.Joke;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface JokeReadEntityMapper {

    @Mapping(source = "text", target = "joke")
    JokeReadDto map(Joke joke);
}