package ru.jokebot.jokes.model.mapper;

import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;
import ru.jokebot.jokes.model.dto.JokeCreateUpdateDto;
import ru.jokebot.jokes.model.entity.Joke;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING,
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface JokeCreateUpdateEntityMapper {

    @Mapping(target = "text", ignore = true)
    Joke map(JokeCreateUpdateDto jokeCreateUpdateDto);

    @AfterMapping
    default void setJokeText(JokeCreateUpdateDto jokeCreateUpdateDto, @MappingTarget Joke joke) {
        joke.setText(jokeCreateUpdateDto.getJokeText().trim());
    }
}