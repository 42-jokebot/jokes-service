package ru.jokebot.jokes.model.mapper;

import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;
import ru.jokebot.jokes.model.dto.CategoryCreateUpdateDto;
import ru.jokebot.jokes.model.entity.Category;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING,
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CategoryCreateUpdateEntityMapper {

    @Mapping(target = "name", ignore = true)
    Category map(CategoryCreateUpdateDto categoryCreateUpdateDto);

    @AfterMapping
    default void setCategoryName(CategoryCreateUpdateDto categoryCreateUpdateDto, @MappingTarget Category category) {
        category.setName(categoryCreateUpdateDto.name().trim());
    }
}