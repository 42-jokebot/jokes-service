package ru.jokebot.jokes.model.dto;

public record JokeReadDto(Long id,
                          String joke,
                          Long categoryId) {
}
