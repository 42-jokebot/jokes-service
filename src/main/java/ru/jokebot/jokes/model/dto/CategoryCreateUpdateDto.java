package ru.jokebot.jokes.model.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

public record CategoryCreateUpdateDto(@NotBlank @Size(min = 2, max = 256) String name,
                                      @NotBlank @Size(min = 2, max = 2) String locale) {
}
