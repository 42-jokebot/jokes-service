package ru.jokebot.jokes.model.dto;

import lombok.Builder;

@Builder
public record JokeSaveMessage(Long jokeId,
                              Long categoryId) {
}
