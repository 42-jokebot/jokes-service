package ru.jokebot.jokes.model.dto;

public record CategoryReadDto(Long id,
                              String categoryName,
                              String locale) {
}
