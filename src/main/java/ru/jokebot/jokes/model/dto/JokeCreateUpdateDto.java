package ru.jokebot.jokes.model.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class JokeCreateUpdateDto {

    @NotEmpty
    @NotBlank
    private String jokeText;

    @NotEmpty
    @NotBlank
    private String categoryName;

    @Size(min = 2, max = 2)
    private String locale;
}
