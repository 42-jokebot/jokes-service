package ru.jokebot.jokes.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Joke implements Serializable {

    private Long id;
    private String text;
    private Long categoryId;
}
