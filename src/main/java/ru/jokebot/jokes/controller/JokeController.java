package ru.jokebot.jokes.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ru.jokebot.jokes.model.dto.JokeCreateUpdateDto;
import ru.jokebot.jokes.model.dto.JokeReadDto;
import ru.jokebot.jokes.service.JokeService;

@RestController
@RequestMapping("api/v1/joke")
@RequiredArgsConstructor
public class JokeController {

    private final JokeService jokeService;

    @GetMapping
    public JokeReadDto handleJokeRequest(@RequestParam Long userId,
                                         @RequestParam String locale) {
        return jokeService.handleJokeRequest(userId, locale);
    }

    @GetMapping("/{category}")
    public JokeReadDto handleJokeRequestByCategory(@PathVariable String category,
                                                   @RequestParam Long userId) {
        return jokeService.handleJokeRequestByCategory(category, userId);
    }

    @GetMapping("/search")
    public JokeReadDto handleJokeSearchRequest(@RequestParam("search") String jokeText) {
        return jokeService.getJokeByText(jokeText);
    }

    @GetMapping("/{id:\\d+}")
    public JokeReadDto getJokeById(@PathVariable Long id) {
        return jokeService.getJokeById(id);
    }

    @PostMapping
    public ResponseEntity<JokeReadDto> createJoke(@Validated @RequestBody JokeCreateUpdateDto jokeDto) {
        JokeReadDto createdJoke = jokeService.createJoke(jokeDto);
        return ResponseEntity.created(
                        ServletUriComponentsBuilder
                                .fromCurrentRequestUri()
                                .path("/{id}")
                                .build(createdJoke.id())
                )
                .body(createdJoke);
    }

    @PutMapping("/{id:\\d+}")
    public ResponseEntity<JokeReadDto> updateJoke(@PathVariable Long id,
                                                  @Validated @RequestBody JokeCreateUpdateDto jokeDto) {
        JokeReadDto updatedJoke = jokeService.updateJokeById(id, jokeDto);
        return ResponseEntity.ok(updatedJoke);
    }

    @DeleteMapping("/{id:\\d+}")
    public ResponseEntity<Void> deleteJoke(@PathVariable Long id) {
        jokeService.deleteJokeById(id);
        return ResponseEntity.noContent()
                .build();
    }
}