package ru.jokebot.jokes.controller.advice;

import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ru.jokebot.jokes.exception.CategoryNotFoundException;
import ru.jokebot.jokes.exception.JokeNotFoundException;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestControllerAdvice(basePackages = "ru.jokebot.jokes.controller")
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({CategoryNotFoundException.class, JokeNotFoundException.class})
    public ResponseEntity<ProblemDetail> handleEntityNotFoundException(RuntimeException exception) {
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(NOT_FOUND, exception.getMessage());
        return ResponseEntity.status(NOT_FOUND)
                .body(problemDetail);
    }
}
