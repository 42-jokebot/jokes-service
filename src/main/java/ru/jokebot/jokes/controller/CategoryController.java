package ru.jokebot.jokes.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ru.jokebot.jokes.model.dto.CategoryCreateUpdateDto;
import ru.jokebot.jokes.model.dto.CategoryReadDto;
import ru.jokebot.jokes.service.CategoryService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/categories")
@RequiredArgsConstructor
public class CategoryController {

    private final CategoryService categoryService;

    @GetMapping
    List<CategoryReadDto> getAllCategories(@RequestParam String locale) {
        return categoryService.getAllCategories(locale);
    }

    @GetMapping("/{id:\\d+}")
    public CategoryReadDto getCategoryById(@PathVariable Long id) {
        return categoryService.getCategoryById(id);
    }

    @PostMapping
    public ResponseEntity<CategoryReadDto> createCategory(@Validated @RequestBody CategoryCreateUpdateDto categoryDto) {
        CategoryReadDto createdCategory = categoryService.createCategory(categoryDto);
        return ResponseEntity.created(
                        ServletUriComponentsBuilder
                                .fromCurrentRequestUri()
                                .path("/{id}")
                                .build(createdCategory.id())
                )
                .body(createdCategory);
    }

    @PutMapping("/{id:\\d+}")
    public ResponseEntity<?> updateCategory(@PathVariable Long id,
                                            @Validated @RequestBody CategoryCreateUpdateDto categoryDto) {
        CategoryReadDto updatedCategory = categoryService.updateCategoryById(id, categoryDto);
        return ResponseEntity.ok(updatedCategory);
    }

    @DeleteMapping("/{id:\\d+}")
    public ResponseEntity<Void> deleteCategory(@PathVariable Long id) {
        categoryService.deleteCategoryById(id);
        return ResponseEntity.noContent()
                .build();
    }
}