package ru.jokebot.jokes.service.impl;

import com.hazelcast.core.HazelcastInstance;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.jokebot.jokes.exception.CategoryNotFoundException;
import ru.jokebot.jokes.exception.EntityNotBeenCreatedException;
import ru.jokebot.jokes.model.dto.CategoryCreateUpdateDto;
import ru.jokebot.jokes.model.dto.CategoryReadDto;
import ru.jokebot.jokes.model.mapper.CategoryCreateUpdateEntityMapper;
import ru.jokebot.jokes.model.mapper.CategoryReadEntityMapper;
import ru.jokebot.jokes.persistence.mapper.CategoryMapper;
import ru.jokebot.jokes.service.CategoryService;
import ru.jokebot.logging.annotation.Logged;

import java.util.List;
import java.util.Optional;

@Service
@Logged
@RequiredArgsConstructor
@CacheConfig(cacheNames = "categories-cache")
@Transactional(readOnly = true)
public class CategoryServiceImpl implements CategoryService {

    private final CategoryMapper categoryMapper;
    private final CategoryReadEntityMapper categoryReadEntityMapper;
    private final CategoryCreateUpdateEntityMapper categoryCreateUpdateEntityMapper;

    private final HazelcastInstance hazelcastInstance;

    @Override
    public int getCategoriesCount() {
        return categoryMapper.getCategoriesCount();
    }

    @Override
    public List<CategoryReadDto> getAllCategories(String locale) {
        return categoryMapper.getAllByLocale(locale).stream()
                .map(categoryReadEntityMapper::map)
                .toList();
    }

    @Cacheable(key = "#id")
    @Override
    public CategoryReadDto getCategoryById(Long id) {
        return categoryMapper.getById(id)
                .map(categoryReadEntityMapper::map)
                .orElseThrow(() -> new CategoryNotFoundException(id));
    }

    @Cacheable(key = "#name")
    @Override
    public Optional<CategoryReadDto> getCategoryByName(String name) {
        return categoryMapper.getByName(name.trim())
                .map(categoryReadEntityMapper::map);
    }

    @CachePut(key = "#result.id")
    @Transactional
    @Override
    public CategoryReadDto createCategory(CategoryCreateUpdateDto categoryCreateUpdateDto) {
        return Optional.of(categoryCreateUpdateDto)
                .map(categoryCreateUpdateEntityMapper::map)
                .map(category -> {
                    categoryMapper.save(category);
                    return category;
                })
                .map(category -> {
                    var categoryReadDto = categoryReadEntityMapper.map(category);
                    hazelcastInstance.getMap("categories-cache").put("name", categoryReadDto);
                    return categoryReadDto;
                })
                .orElseThrow(() -> new EntityNotBeenCreatedException(categoryCreateUpdateDto));
    }

    @CachePut(key = "#result.id")
    @Transactional
    @Override
    public CategoryReadDto updateCategoryById(Long id, CategoryCreateUpdateDto categoryCreateUpdateDto) {
        return Optional.ofNullable(categoryMapper.getById(id))
                .map(entity -> categoryCreateUpdateEntityMapper.map(categoryCreateUpdateDto))
                .map(category -> {
                    categoryMapper.update(id, category);
                    return category;
                })
                .map(category -> {
                    var categoryReadDto = categoryReadEntityMapper.map(category);
                    hazelcastInstance.getMap("categories-cache").put("name", categoryReadDto);
                    return categoryReadDto;
                })
                .orElseThrow(() -> new CategoryNotFoundException(id));
    }

    @CacheEvict(key = "#id")
    @Transactional
    @Override
    public void deleteCategoryById(Long id) {
        categoryMapper.deleteById(id);
    }
}
