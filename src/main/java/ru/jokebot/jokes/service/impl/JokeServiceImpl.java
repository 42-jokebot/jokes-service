package ru.jokebot.jokes.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.jokebot.jokes.client.IntegrationServiceClient;
import ru.jokebot.jokes.client.StatisticsServiceClient;
import ru.jokebot.jokes.exception.CategoryNotFoundException;
import ru.jokebot.jokes.exception.JokeNotFoundException;
import ru.jokebot.jokes.model.dto.CategoryCreateUpdateDto;
import ru.jokebot.jokes.model.dto.CategoryReadDto;
import ru.jokebot.jokes.model.dto.JokeCreateUpdateDto;
import ru.jokebot.jokes.model.dto.JokeReadDto;
import ru.jokebot.jokes.model.dto.JokeSaveMessage;
import ru.jokebot.jokes.model.entity.Joke;
import ru.jokebot.jokes.model.mapper.JokeCreateUpdateEntityMapper;
import ru.jokebot.jokes.model.mapper.JokeReadEntityMapper;
import ru.jokebot.jokes.persistence.mapper.JokeMapper;
import ru.jokebot.jokes.service.CategoryService;
import ru.jokebot.jokes.service.JokeService;
import ru.jokebot.jokes.service.producer.JokeSaveProducer;
import ru.jokebot.logging.annotation.Logged;

import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
@Logged
@RequiredArgsConstructor
@CacheConfig(cacheNames = "jokes-cache")
@Transactional(readOnly = true)
public class JokeServiceImpl implements JokeService {

    private final JokeSaveProducer jokeSaveProducer;
    private final IntegrationServiceClient integrationServiceClient;
    private final StatisticsServiceClient statisticsServiceClient;
    private final CategoryService categoryService;
    private final JokeMapper jokeMapper;
    private final JokeReadEntityMapper jokeReadEntityMapper;
    private final JokeCreateUpdateEntityMapper jokeCreateUpdateEntityMapper;

    @Transactional
    @Override
    public JokeReadDto handleJokeRequest(Long userId, String locale) {
        var notWatchedJoke = handleNotWatchedJokeRequest(userId, locale);
        sendJoke(notWatchedJoke);

        return notWatchedJoke;
    }

    @Cacheable(key = "#userId + '_' + #category")
    @Transactional
    @Override
    public JokeReadDto handleJokeRequestByCategory(String category, Long userId) {
        CategoryReadDto categoryDto = categoryService.getCategoryByName(category)
                .orElseThrow(() -> new CategoryNotFoundException(category));

        return getNotWatchedJokeReadDto(userId, categoryDto);
    }

    @CachePut(key = "#userId + '_' + #locale")
    public JokeReadDto handleNotWatchedJokeRequest(Long userId, String locale) {
        List<CategoryReadDto> topCategories = getTopCategoriesByLocale(locale);
        CategoryReadDto category = selectCategory(topCategories, locale);
        return getNotWatchedJokeReadDto(userId, category);
    }

    public JokeReadDto getNotWatchedJokeReadDto(Long userId, CategoryReadDto categoryDto) {
        List<Long> notWatchedJokes = statisticsServiceClient
                .getNotViewedJokesByCategory(categoryDto.id(), userId);

        if (notWatchedJokes.isEmpty()) {
            var joke = requestJokeFromIntegrationService(categoryDto);
            return jokeMapper.getByText(joke.getJokeText())
                    .map(jokeReadEntityMapper::map)
                    .orElseGet(() -> createJoke(joke));
        }

        Random random = new Random();
        Long jokeId = notWatchedJokes.get(random.nextInt(notWatchedJokes.size()));
        return getJokeById(jokeId);
    }

    private List<CategoryReadDto> getTopCategoriesByLocale(String locale) {
        var categoryReactionMap = statisticsServiceClient.getReactedCategories();
        double averageReaction = categoryReactionMap.values().stream()
                .mapToInt(Integer::intValue)
                .average()
                .orElse(0);

        return categoryReactionMap.entrySet().stream()
                .filter(entry -> entry.getValue() >= averageReaction)
                .map(entry -> categoryService.getCategoryById(entry.getKey()))
                .filter(category -> locale.equals(category.locale()))
                .toList();
    }

    private CategoryReadDto selectCategory(List<CategoryReadDto> topCategories, String locale) {
        Random random = new Random();
        List<CategoryReadDto> allCategoriesByLocale = categoryService.getAllCategories(locale);
        if (topCategories.size() >= allCategoriesByLocale.size() / 2) {
            return topCategories.get(random.nextInt(topCategories.size()));
        }

        return allCategoriesByLocale.get(random.nextInt(allCategoriesByLocale.size()));
    }

    private JokeCreateUpdateDto requestJokeFromIntegrationService(CategoryReadDto category) {
        var joke = integrationServiceClient.requestJoke(category.categoryName(), category.locale());
        joke.setCategoryName(category.categoryName());
        return joke;
    }

    @Cacheable(key = "#jokeText")
    @Override
    public JokeReadDto getJokeByText(String jokeText) {
        return jokeMapper.getByText(jokeText)
                .map(jokeReadEntityMapper::map)
                .orElseThrow(() -> new JokeNotFoundException(jokeText));
    }

    @Cacheable(key = "#id")
    @Override
    public JokeReadDto getJokeById(Long id) {
        return jokeMapper.getById(id)
                .map(jokeReadEntityMapper::map)
                .orElseThrow(() -> new JokeNotFoundException(id));
    }

    @CachePut(key = "#result.id")
    @Transactional
    @Override
    public JokeReadDto createJoke(JokeCreateUpdateDto jokeCreateUpdateDto) {
        String categoryName = jokeCreateUpdateDto.getCategoryName();
        Optional<CategoryReadDto> category = categoryService.getCategoryByName(categoryName);

        Joke joke = jokeCreateUpdateEntityMapper.map(jokeCreateUpdateDto);
        if (category.isPresent()) {
            joke.setCategoryId(category.get().id());
        } else {
            var categoryCreateUpdateDto = new CategoryCreateUpdateDto(categoryName, jokeCreateUpdateDto.getLocale());
            var categoryReadDto = categoryService.createCategory(categoryCreateUpdateDto);
            joke.setCategoryId(categoryReadDto.id());
        }
        jokeMapper.save(joke);

        return jokeReadEntityMapper.map(joke);
    }

    @CachePut(key = "#result.id")
    @Transactional
    @Override
    public JokeReadDto updateJokeById(Long id, JokeCreateUpdateDto jokeCreateUpdateDto) {
        return jokeMapper.getById(id)
                .map(entity -> jokeCreateUpdateEntityMapper.map(jokeCreateUpdateDto))
                .map(joke -> {
                    jokeMapper.update(id, joke);
                    return joke;
                })
                .map(jokeReadEntityMapper::map)
                .orElseThrow(() -> new JokeNotFoundException(id));
    }

    @CacheEvict(key = "#id")
    @Transactional
    @Override
    public void deleteJokeById(Long id) {
        jokeMapper.deleteById(id);
    }

    private void sendJoke(JokeReadDto joke) {
        jokeSaveProducer.sendJokeSaveMessage(
                JokeSaveMessage.builder()
                        .jokeId(joke.id())
                        .categoryId(joke.categoryId())
                        .build());
    }
}
