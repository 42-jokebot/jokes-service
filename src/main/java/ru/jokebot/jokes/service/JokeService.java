package ru.jokebot.jokes.service;

import ru.jokebot.jokes.model.dto.JokeCreateUpdateDto;
import ru.jokebot.jokes.model.dto.JokeReadDto;

public interface JokeService {

    JokeReadDto handleJokeRequest(Long userId, String locale);

    JokeReadDto getJokeByText(String jokeText);

    JokeReadDto getJokeById(Long id);

    JokeReadDto createJoke(JokeCreateUpdateDto jokeDto);

    JokeReadDto updateJokeById(Long id, JokeCreateUpdateDto jokeDto);

    JokeReadDto handleJokeRequestByCategory(String category, Long userId);

    void deleteJokeById(Long id);
}