package ru.jokebot.jokes.service.producer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import ru.jokebot.jokes.model.dto.JokeSaveMessage;
import ru.jokebot.logging.annotation.Logged;

@Service
@Logged
@RequiredArgsConstructor
@Slf4j
public class JokeSaveProducer {

    private final KafkaTemplate<String, Object> kafkaTemplate;

    public void sendJokeSaveMessage(JokeSaveMessage jokeSaveMessage) {
        kafkaTemplate.send("queue.jokes.save", jokeSaveMessage);
    }
}
