package ru.jokebot.jokes.service;

import ru.jokebot.jokes.model.dto.CategoryCreateUpdateDto;
import ru.jokebot.jokes.model.dto.CategoryReadDto;

import java.util.List;
import java.util.Optional;

public interface CategoryService {

    int getCategoriesCount();

    List<CategoryReadDto> getAllCategories(String locale);

    CategoryReadDto getCategoryById(Long id);

    Optional<CategoryReadDto> getCategoryByName(String name);

    CategoryReadDto createCategory(CategoryCreateUpdateDto categoryCreateUpdateDto);

    CategoryReadDto updateCategoryById(Long id, CategoryCreateUpdateDto categoryCreateUpdateDto);

    void deleteCategoryById(Long id);
}
