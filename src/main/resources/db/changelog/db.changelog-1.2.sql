--liquibase formatted sql

--changeset delmark:1
INSERT INTO jokes.category (id, name, locale)
VALUES (14, 'Автомобили', 'ru')
ON CONFLICT (id) DO NOTHING;

--changeset delmark:2
DELETE
FROM jokes.category
WHERE name = 'классические';