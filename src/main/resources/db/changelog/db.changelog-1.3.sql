--liquibase formatted sql

--changeset ilyap:1
ALTER TABLE jokes.joke
    DROP CONSTRAINT joke_category_id_fkey,
    ADD CONSTRAINT joke_category_id_fkey
        FOREIGN KEY (category_id)
            REFERENCES jokes.category (id)
            ON DELETE CASCADE;