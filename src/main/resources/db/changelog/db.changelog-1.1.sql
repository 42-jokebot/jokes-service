--liquibase formatted sql

--changeset ilyap:1
INSERT INTO jokes.category (id, name, locale)
VALUES (1, 'Any', 'en'),
       (2, 'Programming', 'en'),
       (3, 'Misc', 'en'),
       (4, 'Dark', 'en'),
       (5, 'Pun', 'en'),
       (6, 'Spooky', 'en'),
       (7, 'Christmas', 'en'),
       (8, 'политика', 'ru'),
       (9, 'классические', 'ru'),
       (10, 'случайный', 'ru'),
       (11, 'вовочка', 'ru'),
       (12, 'программист', 'ru'),
       (13, 'новогодний', 'ru')
ON CONFLICT (id) DO NOTHING;
