--liquibase formatted sql

--changeset ilyap:1
ALTER TABLE jokes.joke
    ALTER COLUMN text TYPE VARCHAR;